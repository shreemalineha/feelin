import { TouchableOpacity } from "react-native";
const RNTouchableOpacity = ({ ...props }) => <TouchableOpacity {...props} />;
export default RNTouchableOpacity;
