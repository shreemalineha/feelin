import colorSchema from "../colorSchema";

const greenCol = {
  color: colorSchema.greenMain,
};
export default greenCol;
