const button = {
  buttonLarge: {
    padding: 18,
    borderRadius: 25,
    margin: 6,
    marginBottom: 10,
  },
};
export default button;
