import { Text } from "@rneui/themed";

const RNEText = ({ children, ...props }) => <Text {...props}>{children}</Text>;

export default RNEText;
