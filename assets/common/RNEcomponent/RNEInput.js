import { Input } from "@rneui/themed";

const RNEInput = ({ ...props }) => <Input {...props} />;
export default RNEInput;
