import { Button } from "@rneui/base";

const RNEButton = ({ ...props }) => <Button {...props} />;
export default RNEButton;
