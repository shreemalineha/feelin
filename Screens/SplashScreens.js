import { StyleSheet } from "react-native";
import { useEffect, useState } from "react";

import LottieView from "lottie-react-native";
import RNEText from "../assets/common/RNEcomponent/RNEText";
import RNView from "../assets/common/RNcomponent/RNView";
import regLogin from "../assets/common/Text/regLogin";
export default function SplashScreens() {
  const [show, setShow] = useState(false);
  setTimeout(() => {
    setShow(true);
  }, 2000);
  return (
    <RNView style={styles.container}>
      <LottieView
        source={require("../assets/birdSplash.json")} // Replace with your animation JSON file
        autoPlay
        loop={true}
        resizeMode="cover"
        onAnimationFinish={() => setLoading(false)}
      />
      {show ? (
        <RNEText style={styles.splashHead}>{regLogin.feelin}</RNEText>
      ) : null}
    </RNView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  splashHead: {
    fontSize: 50,
    fontWeight: "900",
    color: "white",
    fontFamily: "Futura",
    transform: [{ translateX: -10 }, { translateY: -70 }],
  },
});
