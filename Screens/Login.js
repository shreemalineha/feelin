import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import RNEText from "../assets/common/RNEcomponent/RNEText";
import RNEInput from "../assets/common/RNEcomponent/RNEInput";
import RNEButton from "../assets/common/RNEcomponent/RNEButton";
import RNView from "../assets/common/RNcomponent/RNView";
import RNTouchableOpacity from "../assets/common/RNcomponent/RNTouchableOpacity";
import colorSchema from "../assets/common/enum/colorSchema";
import greenCol from "../assets/common/enum/Styles/greenCol";
import button from "../assets/common/enum/Styles/button";
import regLogin from "../assets/common/Text/regLogin";
const Login = () => {
  const [otp, setOtp] = useState(true);
  const [verify, setVerify] = useState(false);
  const navigation = useNavigation();
  const otpSend = () => {
    setOtp(false);
  };
  return (
    <RNView style={styles.loginContainer}>
      <RNView style={styles.loginTextContainer}>
        <RNEText style={styles.loginText}>
          {regLogin.hey} <RNEText style={greenCol}>!</RNEText>
        </RNEText>

        <RNEText style={styles.loginText}>{regLogin.howyou} </RNEText>
        <RNEText style={[styles.loginText, greenCol]}>
          {regLogin.feelin}
        </RNEText>
      </RNView>
      <RNView style={styles.LogRegFormContainer}>
        <RNEInput label="EMAIL / PHONE" placeholder="Enter email or phone" />
        {!otp ? (
          <>
            <RNEInput label="OTP" placeholder="Enter OTP" />
            <RNEText style={styles.logRegFormTextOTP}>
              {regLogin.didNotRecivedOTP}{" "}
              <RNEText style={greenCol}>
                {regLogin.waitFor} <RNEText style={greenCol}>01:20s</RNEText>
              </RNEText>
              {/* <RNEText style={style.colorGreen}>Send Again</RNEText> */}
            </RNEText>
          </>
        ) : null}
        {otp ? (
          <RNEButton
            title="SEND OTP"
            color={colorSchema.greenMain}
            buttonStyle={button.buttonLarge}
            onPress={otpSend}
          />
        ) : null}

        {!otp ? (
          <RNEButton
            title="VERIFY OTP"
            color={colorSchema.greenMain}
            buttonStyle={button.buttonLarge}
            onPress={() => setVerify(true)}
          />
        ) : null}
        <RNEButton
          title="CONTINUE WITH GOOGLE"
          icon={{
            name: "google-plus-square",
            type: "font-awesome",
            size: 30,
            color: colorSchema.greenMain,
          }}
          color={colorSchema.whiteNormal}
          type="outline"
          titleStyle={{ color: colorSchema.greenMain, marginLeft: 5 }}
          buttonStyle={{
            ...button.buttonLarge,
            borderColor: colorSchema.greenMain,
            borderWidth: 1.5,
          }}
        />
        <RNEText style={styles.logRegFormText}>
          {regLogin.newTofeelin}{" "}
          <RNTouchableOpacity onPress={() => navigation.navigate("Register")}>
            <RNEText style={greenCol}>{regLogin.signUp}</RNEText>
          </RNTouchableOpacity>
        </RNEText>
      </RNView>
    </RNView>
  );
};

export default Login;
const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    marginTop: 150,
    marginLeft: 20,
    marginRight: 20,
  },
  loginText: {
    fontSize: 60,
    fontWeight: 900,
  },
  loginTextContainer: {
    textAlign: "left",
    marginBottom: 40,
  },

  LogRegFormContainer: {
    flex: 1,
  },

  logRegFormText: {
    fontWeight: "bold",
    fontSize: 14,
    textAlign: "center",
    marginTop: 10,
  },
  logRegFormTextOTP: {
    fontWeight: "bold",
    fontSize: 12,
    textAlign: "right",
    marginTop: -10,
    marginBottom: 40,
  },
});
